package task2.IOstream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class IOstreamDemoWithoutBefferedReader {
    public static Logger logger = LogManager.getLogger(IOstreamDemoWithoutBefferedReader.class);
    private static byte item;

    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("src/ReadWriteFileDemo/resources/SampleTextFile_1000kb.txt");
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);

            while (dataInputStream.available() > 0) {
                item = dataInputStream.readByte();
                logger.trace(item);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}

