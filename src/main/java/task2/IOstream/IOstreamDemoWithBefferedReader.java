package task2.IOstream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class IOstreamDemoWithBefferedReader {
    public static Logger logger = LogManager.getLogger(IOstreamDemoWithBefferedReader.class);
    private static char item;

    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("src/ReadWriteFileDemo/resources/SampleTextFile_1000kb.txt");
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream, 512);
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);

            while (dataInputStream.available() > 0) {
                item = dataInputStream.readChar();
                logger.trace(item);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
