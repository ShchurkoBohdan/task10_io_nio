package task4.JavaFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class JavaFileReader {
    public static Logger logger = LogManager.getLogger(JavaFileReader.class);

    public static String getFilePath(){
        Scanner scanner = new Scanner(System.in);
        String filePath;
        logger.info("Enter the path to file: ...");
        filePath = scanner.nextLine();
        return filePath;
    }

    public static void readFile(String path){
        try {
            List<String> file1 = Files.readAllLines(Paths.get(path));
            for (String i : file1){
                System.out.println(i + "\n --------------------------");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readAndWriteFile(){

    }
}
