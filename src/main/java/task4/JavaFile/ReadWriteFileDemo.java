package task4.JavaFile;

import java.io.*;

public class ReadWriteFileDemo {
    public static void main(String[] args) throws IOException {
        File file = new File("src/main/java/task1/de/serialization/SerializationDemo.java");
        File fileToWrite = new File("src/main/resources/writtenFile.txt");
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileToWrite));

        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while (bufferedReader.read() != -1) {
                writer.write((String) bufferedReader.readLine());
                writer.write("\n");
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
