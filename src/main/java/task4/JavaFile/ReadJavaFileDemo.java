package task4.JavaFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class ReadJavaFileDemo {
    public static void main(String[] args) {
        JavaFileReader.readFile(JavaFileReader.getFilePath());
    }
}
