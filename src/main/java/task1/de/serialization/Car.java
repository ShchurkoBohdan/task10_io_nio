package task1.de.serialization;

import com.sun.istack.internal.Nullable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;

public class Car implements Serializable {
    public static Logger logger = LogManager.getLogger(Car.class);
    private Engine engine;
    private int doorsQty;
    private transient Wheel wheel;
    private int wheelQty;
    private int speed;

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public int getDoorsQty() {
        return doorsQty;
    }

    public void setDoorsQty(int doorsQty) {
        this.doorsQty = doorsQty;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public int getWheelQty() {
        return wheelQty;
    }

    public void setWheelQty(int wheelQty) {
        this.wheelQty = wheelQty;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void print(){
        logger.trace("\n Car object: " +
        "\n Engine: \n engine item 1 - " + engine.getItem1() +
        "\n engine item 2 - " + engine.getItem2() +
        "\n engige item 3 - " + engine.getItem3() +
        "\n doorsQty : " + getDoorsQty() +
        "\n Wheel : " + getWheel() +
        "\n wheelQty: " + getWheelQty() +
        "\n speed: " + getSpeed());

    }
}
