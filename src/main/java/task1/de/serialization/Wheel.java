package task1.de.serialization;

import com.sun.istack.internal.Nullable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;

public class Wheel implements Serializable {
    public static Logger logger = LogManager.getLogger(Wheel.class);
    private String tire;
    private String driveWheelType;

    public Wheel(String tire, String driveWheelType) {
        this.tire = tire;
        this.driveWheelType = driveWheelType;
    }


    public String getTire() {
        try{
            return tire;
        }catch (NullPointerException e){
            logger.error("Tire field contains null value.");
        }

        return null;
    }

    public void setTire(String tire) {

        this.tire = tire;
    }

    public String getDriveWheelType() {
        try{
            return driveWheelType;
        }catch (NullPointerException e){
            logger.error("DriveWheelType field contains null value.");
        }

        return null;
    }

    public void setDriveWheelType(String driveWheelType) {

        this.driveWheelType = driveWheelType;
    }
}
