package task1.de.serialization;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.IIOException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationDemo {
    public static Logger logger = LogManager.getLogger(DeserializationDemo.class);
    public static void main(String[] args) {
        Car car = null;

        try{
            FileInputStream fileInputStream = new FileInputStream("src/ReadWriteFileDemo/resources/car.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            car = (Car) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();

        }catch (IOException e){
            e.printStackTrace();
            return;
        }catch (ClassNotFoundException e){
            logger.error("Car class not found");
            e.printStackTrace();
            return;
        }

        car.print();
    }
}
