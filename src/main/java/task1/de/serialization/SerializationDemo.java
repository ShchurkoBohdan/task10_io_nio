package task1.de.serialization;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationDemo {
    // some comment
    public static Logger logger = LogManager.getLogger(SerializationDemo.class);

    public static void main(String[] args) {
        Car car = new Car();
        car.setEngine(new Engine("Engine item 1", "Engine item 2", "Engine item 3"));
        car.setSpeed(0);
        car.setDoorsQty(5);
        car.setWheel(new Wheel("Nokian", "alloy"));
        car.setWheelQty(4);

        try{
            FileOutputStream outputStream = new FileOutputStream("src/ReadWriteFileDemo/resources/car.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(car);
            objectOutputStream.close();
            outputStream.close();
            logger.info("Serialized data is saved in temp/car.ser");

        }catch (IOException e ){
            e.printStackTrace();
        }
    }
}
