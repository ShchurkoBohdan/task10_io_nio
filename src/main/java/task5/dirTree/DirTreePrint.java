package task5.dirTree;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.util.Objects;



public class DirTreePrint {

    public static void printDirTree(File[] arr, int level){
        for (File file : arr) {
            for (int i = 0; i < level; i++)
                System.out.print("\t");

            if(file.isFile())
                System.out.println(file.getName());

            else if(file.isDirectory())
            {
                System.out.println("[" + file.getName() + "]");

                // recursion for sub-directories
                printDirTree(Objects.requireNonNull(file.listFiles()), level + 1);
            }
        }
    }
}

