package task5.dirTree;

import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Enter directory path for starting: ...");
        Scanner scanner = new Scanner(System.in);
        String startDirPath = scanner.nextLine();


        File startDir = new File(startDirPath);

        if(startDir.exists() && startDir.isDirectory()) {
            File arr[] = startDir.listFiles();

            System.out.println("**********************************************");
            System.out.println("Files from main directory : " + startDir);
            System.out.println("**********************************************");

            // Calling recursive method
            DirTreePrint.printDirTree(arr, 0);
        }
    }
}

