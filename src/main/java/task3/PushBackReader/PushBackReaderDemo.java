package task3.PushBackReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import task2.IOstream.IOstreamDemoWithoutBefferedReader;

import java.io.*;

public class PushBackReaderDemo {
    public static Logger logger = LogManager.getLogger(PushbackInputStream.class);
    private static int item;

    public static void main(String[] args) throws IOException {

        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("src/ReadWriteFileDemo/resources/SampleTextFile_1000kb.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        PushbackInputStream pushbackInputStream = new PushbackInputStream(bufferedInputStream);

       while (pushbackInputStream.available() > 0){
           item = pushbackInputStream.read();
           pushbackInputStream.unread(item);
           logger.trace(item);
       }
    }
}
